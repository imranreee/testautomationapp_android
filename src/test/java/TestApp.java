import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestApp {
    WebDriver driver;

    By cancelBtn = By.id("android:id/button2");
    By usernameField = By.id("lblUsername");
    By passwordField = By.id("lblPassword");
    By loginBtn = By.id("btnLogin");
    By successBtn = By.id("btnSuccess");
    By tcFailBtn = By.id("btnBackendError");
    By appCrashBtn = By.id("btnCrashApp");
    By logoutBtn = By.id("btnLogout");
    By invalidPwAlert = By.xpath("//*[@text='Username or password are invalid']");
    By alertOkBtn = By.id("android:id/button3");
    By accountBalanceDialog = By.xpath("//*[@text='Account Balance']");

    String appPackage = "com.orgname.Sample";
    String appActivity = "com.orgname.Sample.Sample";
    String usernameStr = "jorge";
    String passwordStr = "password";

    //For taking current time and date
    DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh-mm-ssaa");
    Date date = new Date();
    String currentDate = dateFormat.format(date);


    @BeforeTest
    public void setUpForAndroid() throws Exception{
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName", "android");
        dc.setCapability("deviceName", "huawei");
        //App won't reset before starting test
        dc.setCapability("noReset", "true");
        //Android access permission dialog will accept automatically
        dc.setCapability("autoGrantPermissions", "true");

        //dc.setCapability("app", appPath);

        dc.setCapability("appPackage", "com.orgname.Sample");
        dc.setCapability("appActivity", "com.orgname.Sample.Sample");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);
        System.out.println("Test Started");
    }


    @Test
    public void testCase1() {
        System.out.println("TC#1 Started");

        waitForVisibilityOf(cancelBtn);
        driver.findElement(cancelBtn).click();
        System.out.println("Clicked on Cancel button");

        waitForVisibilityOf(usernameField);
        driver.findElement(usernameField).sendKeys(usernameStr);
        System.out.println("Username entered");
        driver.findElement(passwordField).sendKeys(passwordStr);
        System.out.println("Password entered");

        ((AppiumDriver)driver).hideKeyboard();

        driver.findElement(loginBtn).click();
        System.out.println("Clicked login button");

        waitForVisibilityOf(successBtn);
        System.out.println("Screen 2 found");

        driver.findElement(logoutBtn).click();
        waitForVisibilityOf(usernameField);
        System.out.println("Logged out successfully");

        System.out.println("TC#1 Passed");
    }

    @Test
    public void testCase2(){
        System.out.println("TC#2 Started");

        Activity activity = new Activity(appPackage, appActivity);
        ((AndroidDriver)driver).startActivity(activity);

        waitForVisibilityOf(cancelBtn);
        driver.findElement(cancelBtn).click();
        System.out.println("Clicked on Cancel button");

        waitForVisibilityOf(usernameField);
        System.out.println("Username filed found");

        driver.findElement(usernameField).sendKeys("invalid");
        System.out.println("Invalid username entered");

        driver.findElement(passwordField).sendKeys(passwordStr);
        System.out.println("Password entered");

        ((AppiumDriver)driver).hideKeyboard();

        driver.findElement(loginBtn).click();
        System.out.println("Clicked login button");

        waitForVisibilityOf(invalidPwAlert);
        System.out.println("Invalid username or password alert found");
        System.out.println("The dialog body is: "+driver.findElement(invalidPwAlert).getText());

        driver.findElement(alertOkBtn).click();
        driver.findElement(usernameField);
        System.out.println("Alert dialog closed");

        System.out.println("TC#2 Passed");
    }

    @Test
    public void testCase3(){
        System.out.println("TC#3 Started");

        Activity activity = new Activity(appPackage, appActivity);
        ((AndroidDriver)driver).startActivity(activity);

        waitForVisibilityOf(cancelBtn);
        driver.findElement(cancelBtn).click();
        System.out.println("Clicked on Cancel button");

        waitForVisibilityOf(usernameField);
        System.out.println("Username filed found");

        driver.findElement(usernameField).sendKeys(usernameStr);
        System.out.println("Invalid username entered");

        driver.findElement(passwordField).sendKeys(passwordStr);
        System.out.println("Password entered");

        ((AppiumDriver)driver).hideKeyboard();

        driver.findElement(loginBtn).click();
        System.out.println("Clicked login button");

        waitForVisibilityOf(successBtn);
        System.out.println("Screen 2 found");

        driver.findElement(successBtn).click();
        waitForVisibilityOf(accountBalanceDialog);
        System.out.println("Account balance dialog found");

        driver.findElement(alertOkBtn).click();
        driver.findElement(successBtn);
        System.out.println("Balance dialog closed");

        System.out.println("TC#3 Passed");
    }

    @Test
    public void testCase4(){
        System.out.println("TC#4 Started");

        Activity activity = new Activity(appPackage, appActivity);
        ((AndroidDriver)driver).startActivity(activity);

        waitForVisibilityOf(cancelBtn);
        driver.findElement(cancelBtn).click();
        System.out.println("Clicked on Cancel button");

        waitForVisibilityOf(usernameField);
        System.out.println("Username filed found");

        driver.findElement(usernameField).sendKeys(usernameStr);
        System.out.println("Invalid username entered");

        driver.findElement(passwordField).sendKeys(passwordStr);
        System.out.println("Password entered");

        ((AppiumDriver)driver).hideKeyboard();

        driver.findElement(loginBtn).click();
        System.out.println("Clicked login button");

        waitForVisibilityOf(successBtn);
        System.out.println("Screen 2 found");

        driver.findElement(tcFailBtn).click();
        waitForVisibilityOf(accountBalanceDialog);

        System.out.println("TC#4 Passed");
    }

    @Test
    public void testCase5(){
        System.out.println("TC#5 Started");

        Activity activity = new Activity(appPackage, appActivity);
        ((AndroidDriver)driver).startActivity(activity);

        waitForVisibilityOf(cancelBtn);
        driver.findElement(cancelBtn).click();
        System.out.println("Clicked on Cancel button");

        waitForVisibilityOf(usernameField);
        System.out.println("Username filed found");

        driver.findElement(usernameField).sendKeys(usernameStr);
        System.out.println("Invalid username entered");

        driver.findElement(passwordField).sendKeys(passwordStr);
        System.out.println("Password entered");

        ((AppiumDriver)driver).hideKeyboard();

        driver.findElement(loginBtn).click();
        System.out.println("Clicked login button");

        waitForVisibilityOf(successBtn);
        System.out.println("Screen 2 found");

        driver.findElement(appCrashBtn).click();
        waitForVisibilityOf(accountBalanceDialog);

        System.out.println("TC#5 Passed");
    }

    @AfterTest
    public void endTest(){
        System.out.println("Test End");
    }

    @AfterMethod
    public void getResult(ITestResult result) throws Exception {
        if (result.getStatus() == ITestResult.FAILURE) {
            String screenShotPath = capture(driver, result.getName() + "_" + currentDate);
        }
    }

    //This method waiting for any element max 30 sec
    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    //This method for taking screenshot
    public static String capture(WebDriver driver, String screenShotName) throws IOException {
        TakesScreenshot ts = (TakesScreenshot)driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String dest = System.getProperty("user.dir") + "\\errorscreenshots\\" + screenShotName + ".png";
        File destination = new File(dest);
        FileUtils.copyFile(source, destination);

        return dest;
    }


}
