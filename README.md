## Project Info

Project Type: `Maven`

Language: `JAVA`

Framework: `Appium`, and `TestNG`

Result Format: `HTML`, and Error Screenshot

IDE: `Intellij IDEA`

Test Environment: `Windows` 

## Test Cases

Test Case #1: Login with valid credentials. username: jorge password: password Expected output: navigate to screen #2

Test Case #2: Login with invalid credentials. Expected Output: Message indicating that credentials are invalid.

Test Case #3: Login -> Navigate to screen 2 -> Press "Success" button. Expected behavior: A message with account balance will be shown. Any other message is an error.

Test Case #4: Login -> Navigate to screen 2 -> Press "Test Case Will Fail" button. Expected Behavior: n/a This button will send the user an error message, this must be recorded as an application bug.

Test Case #5: Login -> Navigate to screen 2 -> Press "App will Crash" button. This is obviously a bug, and should appear as a bug.

